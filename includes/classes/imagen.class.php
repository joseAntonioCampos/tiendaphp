<?php 

class Imagen{
	public $archivo;
	public $ancho;
	public $alto;
	public $redondeada;

	function __construct($archivo,$ancho='100%',$alto=''){
	$this->archivo=$archivo;
	$this->ancho=$ancho;
	$this->alto=$alto;
	$this->redondeada=true;
	}

	function dibujaImagen(){
		if ($this->redondeada==true){
			$c='img-rounded';
		}else{
			$c='';
		}
		$resultado='';
		$resultado.='<img src="'.$this->archivo.'" width="'.$this->ancho.'" high="'.$this->alto.'" class="'.$c.'">';
		return $resultado;
	}
}

 ?>
<?php 

class Jumbotron{
	public $titulo;
	public $texto;
	public $textoEnlace;
	public $urlEnlace;

	function __construct(){
		$this->titulo='';
		$this->texto='';
		$this->textoEnlace='';
		$this->urlEnlace='';

	}

	function pintarJumbotron(){
		$resultado='';
		$resultado.='<br> <div class="jumbotron"> <div class="container">';
		$resultado.='<h1>'.$this->titulo.'</h1>';
		$resultado.='<p>'.$this->texto.'</p>';
		$resultado.='<p><a class="btn btn-primary btn-lg" role="button" href="'.$this->urlEnlace.'">'.$this->textoEnlace.'</a></p>';
		$resultado.='</div>';
		$resultado.='</div>';
		return $resultado;
	}
}


 ?>



 
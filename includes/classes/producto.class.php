
<?php  
//Fichero:   includes/classes/producto.class.php
class Producto{
	//Propiedades del producto
	public $idProd;
	public $nombreProd;
	public $descripcionProd;
	public $precioProd;
	public $unidadesProd;
	public $fechaAlta;
	public $activado;
	public $idCat;
	public $imagenes;
	
	//Metodo constructor
	function __construct($fila){
		global $conexion; //Meto aqui, la conexion a base de datos
		$this->idProd=$fila['idProd'];
		$this->nombreProd=$fila['nombreProd'];
		$this->descripcionProd=$fila['descripcionProd'];
		$this->precioProd=$fila['precioProd'];
		$this->unidadesProd=$fila['unidadesProd'];
		$this->fechaAlta=$fila['fechaAlta'];
		$this->activado=$fila['activado'];
		$this->idCat=$fila['idCat'];
		$this->imagenes=Array();

		$sql="SELECT * FROM imagenes WHERE idProd=".$this->idProd;
		$consulta=mysqli_query($conexion, $sql);
		while($r=mysqli_fetch_array($consulta)){
			$this->imagenes[]=$r['archivoImg'];
		}

	}

	function dimeIva(){
		return $this->precioProd * 0.21;
	}

}




?>
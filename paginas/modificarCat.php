<br>
<h4>Modificar producto</h4>

<?php  
$idCat=$_GET['idCat'];
$sql="SELECT * FROM categorias WHERE idCat=$idCat";
$consulta=mysqli_query($conexion, $sql);
$r=mysqli_fetch_array($consulta);
?>
<h4>Vamos a modificar los datos de la categoría IdCat:<?php echo $idCat; ?></h4>
<form role="form" method="post" action="index.php?p=modificacionCat.php">
  
  <div class="form-group">
    <label for="nombreCat">Nombre de la categoria</label>
    <input type="text" class="form-control" id="nombreCat" name="nombreCat" placeholder="Introduce el nombre de la categoria" value="<?php echo $r['nombreCat'];?>">
  </div>

 
  <div class="form-group">
    <label for="descripcionCat">Descripcion de la categoria</label>
    <textarea class="form-control" rows="3" id="descripcionCat" name="descripcionCat"><?php echo $r['descripcionCat'];?></textarea>
  </div>

  

  <div class="form-group">
    <input type="submit" class="form-control" name="insertarCat" value="Guardar producto">
  </div>

<input type="hidden" name="idCat" value="<?php echo $idCat;?>">

</form>

<br>
<h4>NUEVA NOTICIA</h4>

<form role="form" method="post" action="index.php?p=insercionNoticia.php" enctype="multipart/form-data">
  
  <div class="form-group">
    <label for="tituloNot">Titulo de la noticia</label>
    <input type="text" class="form-control" id="tituloNot" name="tituloNot" placeholder="Introduce el titulo de la nueva noticia">
  </div>

  <div class="form-group">
    <label for="contenidoNot">Contenido de la noticia</label>
    <textarea class="form-control" rows="3" cols="30" id="contenidoNot" name="contenidoNot"></textarea>
  </div>
 
   <div class="form-group">
    <label for="imagenNot">Archivo de imagen</label>
    <input type="file" class="form-control" id="imagenNot" name="imagenNot">
  </div>

  <div class="form-group">
    <input type="submit" class="form-control" name="insertarNot" value="nueva noticia">
  </div>


</form>



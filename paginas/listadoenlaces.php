<ul class="nav nav-tabs">
	<li><a href="index.php?p=listadoenlaces.php&orden=ASC">FECHA Asc</a></li>
	<li><a href="index.php?p=listadoenlaces.php&orden=DESC">FECHA Desc</a></li>
</ul>

<?php  

if(isset($_GET['orden'])){
	$orden=$_GET['orden'];
}else{
	$orden='ASC';
}


switch($orden){
	case 'ASC':
		$ordenMostrar='Ascendente';
		break;
	case 'DESC':
		$ordenMostrar='Descendente';
		break;
}
?>

<h4>Ordenado por <strong>fecha del enlace</strong> de forma <strong><?php echo $ordenMostrar;?></strong></h4>

<?php
//pregunta
//$sql="SELECT * FROM productos ORDER BY $campo $orden LIMIT 0,10";
//$sql="SELECT * FROM productos ORDER BY idProd DESC";

//$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY idProd DESC";

$sql="SELECT * FROM enlaces ORDER BY fechaEnlace $orden";

//ejecutar la consulta
$consulta=mysqli_query($conexion, $sql);
//muestro resultados
while($r=mysqli_fetch_array($consulta)){
	?>
	<article>
		<header>
			
			<h4>Categoria: <?php echo $r['descripcionEnlace'];?></h4>
		</header>
		<section>
			<p><?php echo $r['direccionEnlace'];?></p>
		</section>
		<section>
			<small><?php echo $r['fechaEnlace'];?></small>
		</section>
		<section>
				 
			- <a href="index.php?p=borrarenlace.php&idEnlace=<?php echo $r['idEnlace'];?>" onCLick="if(!confirm('Estas seguro')){return false;};">
				 	Borrar Enlace
				 </a>
			- <a href="index.php?p=modificarenlace.php&idEnlace=<?php echo $r['idEnlace'];?>"onCLick="if(!confirm('Estas seguro')){return false;};">
				 	Modificar Enlace
				 </a>

			
		</section>
			
		
		
	</article>
	<?php
}
?>


<section>
	<h4>
		<ul class="nav nav-pills">
		  <li class="active"><a href="index.php?p=insertarenlace.php" onCLick="if(!confirm('Estas seguro')){return false;};">INSERTAR NUEVO ENLACE</a></li>
		</ul>
	</h4>
</section>
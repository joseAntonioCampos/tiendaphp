<br>
<h4>Modificar producto</h4>

<?php  
$id=$_GET['id'];
$sql="SELECT * FROM productos WHERE idProd=$id";
$consulta=mysqli_query($conexion, $sql);
$r=mysqli_fetch_array($consulta);
?>

<form role="form" method="post" action="index.php?p=modificacion.php">
  
  <div class="form-group">
    <label for="nombreProd">Nombre de producto</label>
    <input type="text" class="form-control" id="nombreProd" name="nombreProd" placeholder="Introduce el nombre de producto" value="<?php echo $r['nombreProd'];?>">
  </div>

  <div class="form-group">
    <label for="precioProd">Precio de producto</label>
    <input type="text" class="form-control" id="precioProd" name="precioProd" placeholder="Introduce el precio del producto" value="<?php echo $r['precioProd'];?>">
  </div>

  <div class="form-group">
    <label for="unidadesProd">Unidades del producto</label>
    <input type="text" class="form-control" id="unidadesProd" name="unidadesProd" placeholder="Introduce las unidades del producto" value="<?php echo $r['unidadesProd'];?>">
  </div>

  <div class="form-group">
    <label for="descripcionProd">Descripcion del producto</label>
    <textarea class="form-control" rows="3" id="descripcionProd" name="descripcionProd"><?php echo $r['descripcionProd'];?></textarea>
  </div>


  <div class="form-group">
    <label for="idCat">Categoria del producto</label>
    <select class="form-control" id="idCat" name="idCat">
      <?php
      $sqlCat="SELECT * FROM categorias ORDER BY nombreCat ASC";
      $consultaCat=mysqli_query($conexion, $sqlCat);
      while($rCat=mysqli_fetch_array($consultaCat)){
        if($rCat['idCat']==$r['idCat']){
          $sel='selected';
        }else{
          $sel='';
        }

        ?>
        <option value="<?php echo $rCat['idCat'];?>" <?php echo $sel;?>>
          <?php echo $rCat['nombreCat'];?>
        </option>
        <?php
      }
      ?>
    </select>
  </div>

 
  <input type="hidden" name="idProd" value="<?php echo $id;?>">

 


</form>

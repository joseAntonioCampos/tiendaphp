<ul class="nav nav-tabs">
	<li><a href="index.php?p=listadocat.php&orden=ASC">Nombre Asc</a></li>
	<li><a href="index.php?p=listadocat.php&orden=DESC">Nombre Desc</a></li>
</ul>

<?php  

if(isset($_GET['orden'])){
	$orden=$_GET['orden'];
}else{
	$orden='ASC';
}


switch($orden){
	case 'ASC':
		$ordenMostrar='Ascendente';
		break;
	case 'DESC':
		$ordenMostrar='Descendente';
		break;
}
?>

<h4>Ordenado por <strong>nombre de categoria</strong> de forma <strong><?php echo $ordenMostrar;?></strong></h4>

<?php
//pregunta
//$sql="SELECT * FROM productos ORDER BY $campo $orden LIMIT 0,10";
//$sql="SELECT * FROM productos ORDER BY idProd DESC";

//$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY idProd DESC";

$sql="SELECT * FROM categorias ORDER BY nombreCat $orden";

//ejecutar la consulta
$consulta=mysqli_query($conexion, $sql);
//muestro resultados
while($r=mysqli_fetch_array($consulta)){
	?>
	<article>
		<header>
			
			<h4>Categoria: <?php echo $r['nombreCat'];?></h4>
		</header>
		<section>
			<p><?php echo $r['descripcionCat'];?></p>
		</section>
		<section>
				 
			- <a href="index.php?p=borrarCat.php&idCat=<?php echo $r['idCat'];?>" onCLick="if(!confirm('Estas seguro')){return false;};">
				 	Borrar Categoria
				 </a>
			- <a href="index.php?p=modificarCat.php&idCat=<?php echo $r['idCat'];?>"onCLick="if(!confirm('Estas seguro')){return false;};">
				 	Modificar Categoria
				 </a>
		</section>
			
		
		
	</article>
	<?php
}
?>